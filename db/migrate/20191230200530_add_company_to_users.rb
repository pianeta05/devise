class AddCompanyToUsers < ActiveRecord::Migration[6.0]
  def change
    add_reference :users, :company, foreign_key: true, default: nil
  end
end
