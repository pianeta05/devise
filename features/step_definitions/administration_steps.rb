When("I create my company") do
    click_on "Create a company"
    @company = FactoryBot.build(:company)
    fill_in "company_name", with: @company.name
    fill_in "company_description", with: @company.description
    fill_in "company_logo", with: @company.logo
    click_on "Save"
end

Then("I should be able to manage my company") do
    visit root_path
    expect(page).to have_content("Edit company")
    expect(page).to have_content("Delete company")
end

Then("I should be associated to a company") do
    visit root_path
    expect(page).to have_content(@company.name)
    expect(page).not_to have_content("Create a company")
    expect(page).not_to have_content("Join a company")
end

Given("There are companies in the system") do
    FactoryBot.create_list(:company, 3)
end

When("I select a company") do
    click_on "Join a company"
    @company = Company.first
    page.find('.list-group-item', text: @company.name).click_on('Join')
end

Then("I should not be able to manage the company") do
    visit root_path
    expect(page).not_to have_content("Edit company")
    expect(page).not_to have_content("Delete company")
end

Given("I am a registered admin") do
    @company = FactoryBot.create(:company)
    @user = FactoryBot.create(:user, admin: true, company: @company)
end

Given("My company has a worker") do
    @worker = FactoryBot.create(:user, company: @company)
end

Then("I should see the list of my company workers") do
    @company.users.each do |worker|
        expect(page).to have_content(worker.name)
    end
end

When("I grant my worker admin priviledges") do
    visit root_path
    page.find('.list-group-item', text: @worker.name).click_on("Make admin")
end

Then("My worker should be able to manage the company") do
  click_on "My account"
  click_on "Sign out"
  click_on "Sign in"
  fill_in "user_email", with: @worker.email
  fill_in "user_password", with: @worker.password
  click_on "Log in"
  visit root_path
  expect(page).to have_content("Edit company")
  expect(page).to have_content("Delete company")
end

Given("My company has another admin") do
    @worker = FactoryBot.create(:user, company: @company, admin: true)
end

When("I ungrant my worker admin priviledges") do
    visit root_path
    page.find('.list-group-item', text: @worker.name).click_on("Make non-admin")
end

Then("My worker should not be able to manage the company") do
    click_on "My account"
    click_on "Sign out"
    click_on "Sign in"
    fill_in "user_email", with: @worker.email
    fill_in "user_password", with: @worker.password
    click_on "Log in"
    visit root_path
    expect(page).to have_content(@company.name)
    expect(page).not_to have_content("Edit company")
    expect(page).not_to have_content("Delete company")
end
