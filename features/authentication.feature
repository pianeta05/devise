Feature: Authentication
  In order to use the app
  As an user
  I want to authenticate

Scenario: Sign up
  Given I am not signed in
  When I fill the sign up form
  Then I should see that my account is created

Scenario: Sign in
  Given I am a registered user
  And I am not signed in
  When I fill the sign in form
  Then I should be signed in

Scenario: Log out
  Given I am a registered user
  And I fill the sign in form
  When I sign out 
  Then I should be signed out

Scenario: Edit my profile
  Given I am a registered user 
  And I fill the sign in form 
  When I edit my profile 
  Then I should see the changes

Scenario: Change my password
  Given I am a registered user 
  And I fill the sign in form 
  When I edit my password 
  And I sign out
  And I sign in with the new password
  Then I should be signed in