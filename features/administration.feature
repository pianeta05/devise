Feature: Administrarion
  In order to manage my company
  As a manager
  I want to register as an admin

Scenario: Signing up as an admin
    Given I am not signed in
    When I fill the sign up form
    And I create my company
    Then I should be able to manage my company
    And I should be associated to a company

Scenario: Signing up as a worker
    Given I am not signed in
    And There are companies in the system
    When I fill the sign up form
    And I select a company
    Then I should not be able to manage the company
    And I should be associated to a company

Scenario: Checking the workers list
    Given I am a registered admin
    And My company has a worker
    And I fill the sign in form
    Then I should see the list of my company workers

Scenario: Granting admin priviledges
    Given I am a registered admin
    And My company has a worker
    And I fill the sign in form
    When I grant my worker admin priviledges
    Then My worker should be able to manage the company

Scenario: Ungranting admin priviledges
    Given I am a registered admin
    And My company has another admin
    And I fill the sign in form
    When I ungrant my worker admin priviledges
    Then My worker should not be able to manage the company
