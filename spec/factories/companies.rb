# == Schema Information
#
# Table name: companies
#
#  id          :integer          not null, primary key
#  name        :string
#  logo        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryBot.define do
    factory :company do
        name                  { Faker::Company.name }
        description           { Faker::Lorem.sentence(word_count: 10) }
        logo                  { Faker::LoremPixel.image(size: "150x150") }
    end
end
