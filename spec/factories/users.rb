# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  name                   :string
#  image                  :string
#  description            :text
#  admin                  :boolean          default(FALSE)
#  company_id             :integer
#

FactoryBot.define do
  factory :user do
    email                 { Faker::Internet.unique.email }
    name                  { Faker::Name.name }
    description           { Faker::Lorem.sentence(word_count: 10) }
    image                 { Faker::LoremPixel.image(size: "150x150") }
    password              { "password" }
    password_confirmation { "password" }
  end
end
